import re

from User import User, UserPassword
from password_databse import PasswordRepository, SQLitePasswordDB


DB_PATH = 'db.sqlite'

NUMBER_MIN_LIMIT = 5
ALPHABET_MIN_LIMIT = 10


def validate_number_range(pass_phrase_number):
    regex = r'(\d)'
    return len(re.findall(regex, pass_phrase_number)) >= NUMBER_MIN_LIMIT

def validate_alphabet_range(pass_phrase_alphabet):
    regex = r'([a-zA-Z])'
    return len(re.findall(regex, pass_phrase_alphabet)) >= ALPHABET_MIN_LIMIT

def validate_special_char(special_char):
    chars = ''.join(special_char.split(','))
    regex = r'([^a-zA-Z0-9\,])'

    special_char_length = len(re.findall(regex, special_char))
    return len(chars) and special_char_length


# def validate_pass_phrase(num, alphabet, special_char):

#     valid = False

#     valid = validate_number_range(num) and validate_alphabet_range(alphabet) and validate_special_char(special_char)

#     return valid


valid_pass_phrase_conditions = """
    Valid Pass Phase condition:

        Number Range - (cant be less than 5 num)
        Alphabet Range - (cant be less than 10 char)
        Special Char Except - (comma separated values, only will take special chars)

"""



def create(userPassword):
    pass_phrase = None

    print(valid_pass_phrase_conditions)

    # TODO : clean it up
    while True:
        numeric_pass_phrase = input("Pass Phrase Number Range - (cant be less than 5 num) : ")
        if validate_number_range(numeric_pass_phrase):
            break

    
    while True:
        alphabet_pass_phrase = input("Alphabet Range - (cant be less than 10 char) : ")
        if validate_alphabet_range(alphabet_pass_phrase):
            break

    while True:
        special_char_pass_phrase = input("Special Char Except - (comma separated values, only will take special chars) : ")
        if validate_special_char(special_char_pass_phrase):
            break

    pass_phrase = numeric_pass_phrase + alphabet_pass_phrase + ''.join(special_char_pass_phrase.split(',')) 
    passwords = userPassword.generate(pass_phrase)
    userPassword.print()

def print_password(phrase, medium, strong, very_strong):

    print(f'Pass Phrase             : {phrase}')
    print(f'1. Medium Password      : {medium}')
    print(f'2. Strong Password      : {strong}')
    print(f'3. Very Strong Password : {very_strong}')
    print()    

def recover(userPassword):
    pass_phrase = None

    while pass_phrase is None:
        pass_phrase = input("Pass Phrase : ")

    passwords = userPassword.recover(pass_phrase)

    try:
        length = len(passwords)
        if(length):
            print(f"Total {length} password recovered ")
            for password in passwords:
                pass_phrase, m_pass, s_pass, vs_pass = password
                print_password(pass_phrase, m_pass, s_pass, vs_pass)
    except Exception as e:
        # print(e) # TODO : read/check other error and fix accordingly then use appropriate exception
        pass # got NoneType exception when no password with given pass_phrase
        



def ask_user_email():
    # did not validate email
    # because,application that will use
    # this `password generator` already done validation
    # this is for presentation purpose only 
    email = None
    while email is None:
        email = input("Email : ")
    return email

def promot():
    print('='*30)

    email = ask_user_email()


    user = User(email)

    db = SQLitePasswordDB(DB_PATH)
    password_repository = PasswordRepository(db)
    userPassword = UserPassword(user, password_repository)


    run = True

    while run:
        response = None
        while response not in [1, 2, 3]:
            print("\nOperations : ")

            response = int(input("\t[1] Create Password\n\t[2] Recover  Password\n\t[3] Exit\n"))

        if response == 1:
            create(userPassword)
        if response == 2:
            recover(userPassword)
        if response == 3:
            run = False

    
    print('='*30)
    


if __name__ == "__main__":

    promot()



