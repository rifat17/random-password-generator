import abc
import sqlite3

from password_constants import(
    MEDIUM_PASSWORD,
    STRONG_PASSWORD,
    VERYSTRONG_PASSWORD,
    PASS_PHRASE,
    USER,

)

PASSWORD_TABLE = 'password'


class AbstractPasswordRepository(abc.ABC):

    @abc.abstractmethod
    def get_password(self, value):
        raise NotImplementedError

    @abc.abstractmethod
    def add_password(self, value):
        raise NotImplementedError



class AbstractPasswordDatabase(abc.ABC):

    @abc.abstractmethod
    def select(self, value):
        raise NotImplementedError

    # @abc.abstractmethod
    # def update(self, value):
    #     raise NotImplementedError

    @abc.abstractmethod
    def insert(self, value):
        raise NotImplementedError


class PasswordRepository(AbstractPasswordRepository):

    def __init__(self, db : AbstractPasswordDatabase):
        self._db = db

    def get_password(self, value):


        user = value[USER]
        email = user.email()
        
        pass_phrase = value[PASS_PHRASE]


        sql = f'''SELECT {PASS_PHRASE}, {MEDIUM_PASSWORD}, {STRONG_PASSWORD}, {VERYSTRONG_PASSWORD} FROM {PASSWORD_TABLE} 
        WHERE 
        email = ? AND 
        pass_phrase LIKE ?'''
        pass_phrase = f'%{pass_phrase}%'
        # print(sql)

        query_value = (sql, (str(email),(pass_phrase)))

        return self._db.select(query_value)

    def add_password(self, value):


        user = value[USER]
        pass_phrase = value[PASS_PHRASE]

        sql = f'''INSERT OR REPLACE INTO {PASSWORD_TABLE}(
            "email",
            "{PASS_PHRASE}",
            "{MEDIUM_PASSWORD}",
            "{STRONG_PASSWORD}",
            "{VERYSTRONG_PASSWORD}"
        ) VALUES(?,?,?,?,?);'''
        email = user.email()

        # TODO: need to refactor password
        # encapsulate in a class?
        passwds = user.passwords[pass_phrase]

        m_pass = passwds[MEDIUM_PASSWORD]
        s_pass = passwds[STRONG_PASSWORD]
        vs_pass = passwds[VERYSTRONG_PASSWORD]

        query_value = (str(sql), (str(email), str(pass_phrase), str(m_pass), str(s_pass), str(vs_pass)))

        self._db.insert(query_value)







class SafeCursor:
    def __init__(self, connection):
        self.con = connection

    def __enter__(self):
        self.cursor = self.con.cursor()
        return self.cursor

    def __exit__(self, typ, value, traceback):
        self.cursor.close()


class SQLitePasswordDB(AbstractPasswordDatabase):
    def __init__(self, db_path):
        self._conn = sqlite3.connect(db_path)
        self._create_table()

    def _create_table(self):
        with SafeCursor(self._conn) as cursor:
            cursor.execute(f"""CREATE TABLE IF NOT EXISTS {PASSWORD_TABLE}(
                email VARCHAR(50),
                {PASS_PHRASE} TEXT NOT NULL,
                {MEDIUM_PASSWORD} TEXT,
                {STRONG_PASSWORD} TEXT,
                {VERYSTRONG_PASSWORD} TEXT,
                PRIMARY KEY(email, {PASS_PHRASE})
                );
                """)
            self._conn.commit()


    def insert(self, value):

        sql, params = value

        with SafeCursor(self._conn) as cursor:

            cursor.execute(
                sql, params
            )
            self._conn.commit() # do we need this? # (few hours later)yes, we need this -_-



    def select(self, value):

        sql, params = value

        with SafeCursor(self._conn) as cursor:
            cursor.execute(sql, params)
            value_from_db = cursor.fetchall()
            return value_from_db


