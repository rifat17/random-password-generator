from collections import defaultdict

from password_generator import (
    Password,
    MediumPasswordGenerator,
    StrongPasswordGenerator,
    VeryStrongPasswordGenerator,
)
from password_constants import(
    MEDIUM_PASSWORD,
    STRONG_PASSWORD,
    VERYSTRONG_PASSWORD,
    USER,
    PASS_PHRASE,
)


from password_databse import AbstractPasswordRepository


class User:
    def __init__(self, email):
        self._email = email
        self._passwds = {}

    def email(self):
        return self._email

    @property
    def passwords(self):       
        return self._passwds

    @passwords.setter
    def passwords(self, value):
        self._passwds.update(value)

    def __str__(self):
        return f"User : {self._email}\nPasswords : {self.passwords}"


class UserPassword:

    def __init__(self, user : User, passwdrepo : AbstractPasswordRepository):
        self._user = user
        self._repo = passwdrepo

    def generate(self, pass_phrase):
        
        generated_passwords = defaultdict(dict)

        password_engine = Password()

        generators = [
            (MEDIUM_PASSWORD, MediumPasswordGenerator),
            (STRONG_PASSWORD, StrongPasswordGenerator),
            (VERYSTRONG_PASSWORD, VeryStrongPasswordGenerator),
        ]

        for name, generator in generators:
            password_engine.generator = generator()
            passwd = password_engine.generate(pass_phrase)
            pass_dict = {name:passwd}
            generated_passwords[pass_phrase].update(pass_dict)

        self._user.passwords = generated_passwords

        value = {
            USER : self._user,
            PASS_PHRASE : pass_phrase
        }

        self._repo.add_password(value)

        return generated_passwords


    def recover(self, pass_phrase):
        trial = 0
        while True:
            value = {
                USER : self._user,
                PASS_PHRASE : pass_phrase
            }
            passwds = self._repo.get_password(value)
            if passwds:
                return passwds
            trial += 1
            if trial >= 3:
                return None
            pass_phrase = input('Wrong Pass Phrase, Enter your correct pass phrase : ')


    def print(self):

        for name, values in self._user.passwords.items():
            print("Pass Phrase".ljust(20),': ', name)
            for level, passwd in values.items():
                print(level.capitalize().ljust(20), ': ', passwd)
        