import abc

import sqlite3

class AbstractPasswordRepository(abc.ABC):

    @abc.abstractmethod
    def get_password(self, email):
        raise NotImplementedError

    @abc.abstractmethod
    def update_password(self, email, value):
        raise NotImplementedError

class PasswordRepository(AbstractRepository):

    def __init__(self, db : AbstractPasswordDatabase):
        self._db = db

    def get_password(self, email):
        return self._db.select(email)


class AbstractPasswordDatabase(abc.ABC):

    @abc.abstractmethod
    def select(self, value):
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, value):
        raise NotImplementedError

    @abc.abstractmethod
    def insert(self, value):
        raise NotImplementedError


class SQLPasswordDatabase(AbstractPasswordDatabase):
    
    def __init__(self, db_path):
        self._conn = sqlite3.connect(db_path)
        self._cursor = self._conn.cursor()

    def select(self, value):
        query = f"""
        SELECT medium_pass, strong_pass, strong_pass
        FROM userpassword
        WHERE email = {value};
        """
        return self._cursor.execute(query)

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        # can test for type and handle different situations
        self.close()

    def complete(self):
        self._complete = True

    def close(self):
        if self.conn:
            try:
                if self._complete:
                    self.conn.commit()
                else:
                    self.conn.rollback()
            except Exception as e:
                raise StoreException(*e.args)
            finally:
                try:
                    self.conn.close()
                except Exception as e:
                    raise StoreException(*e.args)




