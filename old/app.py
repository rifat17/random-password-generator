from password import UserPassword


def create_pass(userpass, passwds):
    userpass.promot_for_pass_phrase()
    passwds = userpass.generate()
    userpass.print()
    choise = userpass.promot_pass_choice()

    userpass.passwd = passwds[choise]
    print(f'Your Password : {userpass.passwd}')
    

def recover_pass(userpass):
    if userpass.pass_phrase is None:
        print('No password saved to recover.')
        return
    pass_phrase = input('Enter your pass phrase : ')

    passwd = userpass.recover(pass_phrase)
    if passwd is None:
        print('Wrong Pass Phrase, Please Try Again Later!')
    else:
        print(f"Your password is {passwd}")

def promot():
    print('='*30)

    userpass = UserPassword()
    passwords = None

    run = True

    while run:
        response = None
        while response not in [1, 2, 3]:
            print("\nOperations : ")

            response = int(input("\t[1] Create Password\n\t[2] Recover  Password\n\t[3] Exit\n"))

        if response == 1:
            create_pass(userpass, passwords)
        if response == 2:
            recover_pass(userpass)
        if response == 3:
            run = False

    
    print('='*30)
    


if __name__ == "__main__":

    promot()



