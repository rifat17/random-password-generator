from password_generator import (
    Password,
    MediumPasswordGenerator,
    StrongPasswordGenerator,
    VeryStrongPasswordGenerator,
)
from enum import Enum

class PasswordLevel(Enum):
    MEDIUM = 1
    STRONG = 2
    VERY_STRONG = 3


class UserPassword:

    # password_choice = {
    #     PasswordLevel.MEDIUM : '',
    #     PasswordLevel.STRONG : '',
    #     PasswordLevel.VERY_STRONG : '',
    # }
    # choice = list(range(1, len(password_choice)+1))


    password_choice = 3
    choice = list(range(1, password_choice + 1))


    def __init__(self, storage=None):
        self._passwd = None
        self._pass_phrase = None
        self._storage = storage

    # def promot_input(self):
    #     got_pass_phrase = False
    #     while got_pass_phrase is False:
    #         self.pass_phase = input()
    #         if (len(self.pass_phase)) > 0:
    #             got_pass_phrase = True


    @property
    def pass_phrase(self):
        return self._pass_phrase

    @property
    def passwd(self):
        return self._passwd
    
    @passwd.setter
    def passwd(self, value):
        self._passwd = value


    def promot_for_pass_phrase(self):
        if self._passwd:
            # TODO : ask to delete old password
            pass
        self._pass_phrase = input("Please enter your pass phrase : ")

    def generate(self):

        passwds = {}

        passwd = Password()

        medium_gen = MediumPasswordGenerator()
        strong_gen = StrongPasswordGenerator()
        very_strong_gen = VeryStrongPasswordGenerator()

        passwd.generator = medium_gen
        self.medium_pass = passwd.generate(self._pass_phrase)

        passwds.update({ PasswordLevel.MEDIUM.value : self.medium_pass})
        

        passwd.generator = strong_gen
        self.strong_pass = passwd.generate(self._pass_phrase)
        
        passwds.update({PasswordLevel.STRONG.value : self.strong_pass})
        


        passwd.generator = very_strong_gen
        self.very_strong_pass = passwd.generate(self._pass_phrase)
        passwds.update({PasswordLevel.VERY_STRONG.value : self.very_strong_pass})

        self.passwds = passwds
        return self.passwds


    def recover(self, pass_phrase):
        trial = 0
        while True:
            if self.pass_phrase == pass_phrase:
                return self.passwd
            trial += 1
            if trial >= 3:
                return None
            pass_phrase = input('Wrong Pass Phrase, Enter your correct pass phrase : ')




    def print(self):

        print(f'1. Medium Password : {self.medium_pass}')
        print(f'2. Strong Password : {self.strong_pass}')
        print(f'3. Very Strong Password : {self.very_strong_pass}')

    def promot_pass_choice(self):
        print("Choose a password ")
        response = None
        while response not in self.choice:
            response = int(input("[1] Medium, [2] Strong [3] Very Strong : "))

        return response






