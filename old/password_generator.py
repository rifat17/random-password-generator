from abc import ABC, abstractclassmethod
import random
import string

MEDIUM_PASS_EXTRA_CHAR_LENGTH = 5
STRONG_MEDIUM_PASS_EXTRA_CHAR_LENGTH = 10
VERY_STRONG_MEDIUM_PASS_EXTRA_CHAR_LENGTH = 15




class PasswordGeneratable(ABC):
    
    @abstractclassmethod
    def generate_password(pass_phrase: str):
        pass

class PasswordGeneraorNotProvided(Exception):
    def __init__(self, message="Password Generator class not provided."):
        self.message = message
        super().__init__(self.message)

class PassPhaseNotProvided(Exception):
    def __init__(self, message="Pass Phase is Empty."):
        self.message = message
        super().__init__(self.message)


class MediumPasswordGenerator(PasswordGeneratable):

    def generate_password(self, pass_phrase: str):
        extra_char = string.digits
        new_pass_ph = pass_phrase + ''.join(random.sample(extra_char, MEDIUM_PASS_EXTRA_CHAR_LENGTH))
        return ''.join(random.sample(new_pass_ph, len(new_pass_ph)))


class StrongPasswordGenerator(PasswordGeneratable):

    def generate_password(self, pass_phrase:str):

        extra_char = string.ascii_letters
        new_pass_ph = pass_phrase + ''.join(random.sample(extra_char, STRONG_MEDIUM_PASS_EXTRA_CHAR_LENGTH))
        return ''.join(random.sample(new_pass_ph, len(new_pass_ph)))


class VeryStrongPasswordGenerator(PasswordGeneratable):

    def generate_password(self, pass_phrase:str):
        
        extra_char = string.punctuation
        new_pass_ph = pass_phrase + ''.join(random.sample(extra_char, VERY_STRONG_MEDIUM_PASS_EXTRA_CHAR_LENGTH))
        return ''.join(random.sample(new_pass_ph, len(new_pass_ph)))


class Password:

    # def __init__(self, generator : PasswordGeneratable ):
    #     self._generator = generator

    def generate(self, pass_phrase: str):
        if self._generator is None:
            raise PasswordGeneraorNotProvided()
        if( len(pass_phrase) ) == 0:
            raise PassPhaseNotProvided()
        return self._generator.generate_password(pass_phrase)

    def _set_generator(self, generator):
        if not isinstance(generator, PasswordGeneratable):
            raise PasswordGeneraorNotProvided("Please provide a password generator")
        self._generator = generator

    generator = property(fset=_set_generator)