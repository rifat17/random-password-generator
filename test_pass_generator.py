from password_generator import (
    MediumPasswordGenerator,
    StrongPasswordGenerator,
    VeryStrongPasswordGenerator,
    Password,
    PassPhaseNotProvided,
    PasswordGeneraorNotProvided,
)
import unittest


class TestPassword(unittest.TestCase):
    def setUp(self):
        # self.m_pass_generator = MediumPasswordGenerator()
        # self.s_pass_generator = StrongPasswordGenerator()
        # self.vs_pass_generator = VeryStrongPasswordGenerator()

        self.password = Password()

    def test_medium_password_return_string_if_input_string_is_not_empty(self):
        passwd = "password"
        m_pass_generator = MediumPasswordGenerator()
        self.password.generator = m_pass_generator
        m_pass = self.password.generate(passwd)

        self.assertIsNotNone(m_pass)

    def test_medium_password_raise_PassPhaseNotProvided_Exception_if_input_string_is_empty(self):
        passwd = ""
        m_pass_generator = MediumPasswordGenerator()
        self.password.generator = m_pass_generator
        with self.assertRaises(PassPhaseNotProvided) as context:
    
            m_pass = self.password.generate(passwd)
            self.assertEqual('Pass Phase is Empty.', str(context.exception))

    def test_medium_password_raise_PasswordGeneraorNotProvided_if_a_pass_generator_not_provided(self):
        passwd = "password"
        m_pass_generator = 'MediumPasswordGenerator'
        with self.assertRaises(PasswordGeneraorNotProvided) as context:
            self.password.generator = m_pass_generator
            m_pass = self.password.generate(passwd)
            self.assertEqual('Please provide a password generator', str(context.exception))


def main():

    mypass = "Bangladesh"

    m_pass_generator = MediumPasswordGenerator()
    s_pass_generator = StrongPasswordGenerator()
    vs_pass_generator = VeryStrongPasswordGenerator()

    password = Password()

    password.generator = m_pass_generator

    print(f'Medium Password : {password.generate(mypass)}')

    password.generator = s_pass_generator

    print(f'Strong Password : {password.generate(mypass)}')

    password.generator = vs_pass_generator

    print(f'Very Strong Password : {password.generate(mypass)}')

if __name__ == "__main__":
    main()

# python -m unittest test_pass_generator.py